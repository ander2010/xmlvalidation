﻿using Application.Contracts;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;

namespace xmlvaliadion.Controllers
{
    [Route("api/xml")]
    public class XmlController: Controller
    {
        private readonly IXmlValidationService xmlValidationService;

        public XmlController(IXmlValidationService xmlValidationService)
        {
            this.xmlValidationService = xmlValidationService;
        }

        [HttpPost]
        public IActionResult Post([FromBody]string rfc, [FromBody]string cfdi)
        {
            try
            {
              
                xmlValidationService.Validate(rfc, cfdi);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok();
        }
    }
}
