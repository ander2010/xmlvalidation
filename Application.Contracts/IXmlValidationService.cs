﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Application.Contracts
{
    public interface IXmlValidationService
    {
        void Validate(string rfc, string cfdi);
    }
}
