﻿using Application.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Application.Implmentation
{
    public class XmlValidationService : IXmlValidationService
    {
        public void Validate(string rfc, XmlDocument cfdi)
        {
            var targetEmitterNode= cfdi.GetElementsByTagName("Emisor")[0];
            if (targetEmitterNode.InnerText != rfc)
            {
                throw new ArgumentException("The provided rfc does not match the xml input");
            }

            var targetInvoiceNode = cfdi.GetElementsByTagName("Comprobante")[0];
            if (targetInvoiceNode.Attributes["version"].InnerText != "3.3")
            {
                throw new ArgumentException("The provided version is not valid");
            }

            var subTotal = Decimal.Parse(targetInvoiceNode.Attributes["SubTotal"].InnerText);
            var targetConceptNodes = cfdi.GetElementsByTagName("Concepto");
            decimal conceptSum = 0;
            foreach (XmlNode node in targetConceptNodes)
            {
                conceptSum += Decimal.Parse(node.Attributes["Importe"].InnerText);
            }

            if (conceptSum != subTotal)
            {
                throw new ArgumentException("The subtotal value does not match the import values");
            }

        }
    }
}
